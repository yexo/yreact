import React from 'react';
import Badge from 'react-bootstrap/Badge';

class Cat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.data.id,
      url: props.data.url,
      vote: props.data.vote,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  handleClick() {
    // Vote
    const data = JSON.parse(localStorage.getItem('cats'));
    const index = data.findIndex(element => element.id === this.state.id);
    data[index].vote = this.state.vote + 1;
    this.setState({ vote: this.state.vote + 1 }, this.saveStateToLocalStorage(data));
    //console.log(this.state)

    // Next Cat
    const nextCat = data[this.getRandomInt(data.length)];
    if (nextCat.id !== this.state.id) {
      this.setState({ id: nextCat.id });
      this.setState({ url: nextCat.url });
      this.setState({ vote: nextCat.vote });   
    }
    
  }

  saveStateToLocalStorage = (cats) => {
    localStorage.setItem('cats', JSON.stringify(cats))
  }

  render() {
    // console.log(this.props)
    return (
      <div
        className="imageCat"
        style={{ backgroundImage: `url(${this.state.url})` }}
        onClick={this.handleClick}
      >
        <Badge className="vote" variant="secondary">{this.state.vote}</Badge>
      </div>
    );
  }
}

export default Cat