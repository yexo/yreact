import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import Cat from './Cat';

class Vote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cats: props.cats,
    };
    this.nextCat = this.nextCat.bind(this);
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  nextCat() {
    const data = JSON.parse(localStorage.getItem('cats'))
    const index = data.findIndex(element => element.id === this.state.id);
    data[index].vote = this.state.vote + 1

    this.setState({ vote: this.state.vote + 1},
    this.saveStateToLocalStorage(data));    
  }


  render() {
    //console.log(this.props.cats)
    if(this.props.cats.length === 0) {
      return(
        <Alert variant="light">
          Il n'y a pas un chat ici.
        </Alert>
      );
    }
    return (
      <Row>
        <Col xs="12" className="mb-4 mt-4 text-center text-light">
          <h1>Quel est le chat le plus mignon?</h1>
        </Col>
        <Col xs="6" className="mt-2 text-center">
          <Cat onclick={this.nextCat} data={this.props.cats[this.getRandomInt(this.props.cats.length)]} />
        </Col>
        <Col xs="6" className="mt-2 text-center">
          <Cat onclick={this.nextCat} data={this.props.cats[this.getRandomInt(this.props.cats.length)]} />
        </Col>
      </Row>
    );
  }
}

export default Vote;

        