import React from 'react';
import Badge from 'react-bootstrap/Badge';

class Cat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: props.data.url,
      vote: props.data.vote,
    };
  }

  render() {
    // console.log(this.props)
    return (
      <div
        className="imageCat"
        style={{ backgroundImage: `url(${this.state.url})` }}
      >
        <Badge className="vote" variant="secondary">{this.state.vote}</Badge>
      </div>
    );
  }
}

export default Cat