import React from 'react';
import ItemList from './ItemList';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function List({ cats }) {

  if(cats.length === 0) {
    return(
      <Alert variant="light">
        Il n'y a pas un chat ici.
      </Alert>
    );
  }

  return (
    <Row>
      <Col xs="12" className="mb-4 mt-4 text-center text-light">
        <h1>Classement</h1>
      </Col>
      <Col xs="4" className="text-center text-warning">
          <h3>Top 1</h3>
      </Col>
      <Col xs="4" className="text-center">
          <h3>Top 2</h3>
      </Col>
      <Col xs="4" className="text-center">
          <h3>Top 3</h3>
      </Col>
      {cats
        .sort((a,b) => (a.vote < b.vote ? 1 : -1))
        .map(cat => (
        <Col xs="4" key={cat.id} className="mb-4 mt-2" >
          <ItemList data={cat} />
        </Col>
      ))}
    </Row>
  );
}

export default List;