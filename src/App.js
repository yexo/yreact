import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import List from './components/List';
import Vote from './components/Vote';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const FetchCats = async() => {
  try {
    const results = await Axios.get('https://cataas.com/api/cats?tags=cute');
    return results.data.map(result => (
      {
        id: result.id,
        url: "https://cataas.com/cat/" + result.id,
        vote: 0,
      }
    ));
  } catch(e) {
      console.error(e);
    return [];
  } 
}

function App() {

  const [cats, setCats] = useState([]);

  useEffect(() => {
      const localStorageCats = localStorage.getItem('cats');
      //console.log(localStorageCats)
      if (localStorageCats){
          setCats(JSON.parse(localStorageCats));
      } else {
        FetchCats().then(newCats =>{ setCats( oldCat => [ ...oldCat, ...newCats]) });

      }
  }, []);

  useEffect(() => {
    localStorage.setItem('cats', JSON.stringify(cats));
      
  }, [cats]);

  return (
    <Router>
      <Navbar bg="light" variant="light" className="mb-4">
        <Navbar.Brand href="/">Tin'cats</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="/">Vote</Nav.Link>
        <Nav.Link href="/classement">Classement</Nav.Link>
      </Nav>
    </Navbar>
      <Container>
        <Switch>
          <Route exact path="/">
            <Vote cats={ cats }/>
          </Route>
          <Route path="/classement">
            <List cats={ cats }/>
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
