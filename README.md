# Appli web de "classement du chat le plus mignon". (https://gitlab.com/yexo/yreact)

react-router-dom - axios - api https://cataas.com/ - react-bootstrap -

Sujet : https://www.notion.so/Rattrapage-React-YNOV-2021-77cbdade161e468982094f90f8c0dc47

## Pour lancer le projet simplement

### `yarn start`

[http://localhost:3000](http://localhost:3000)

## Hébergement Netlify

[https://dev-thibaut.netlify.app](https://dev-thibaut.netlify.app)

## L'application propose deux interfaces :

- "/vote" => Cette interface affiche deux images de chats. L'utilisateur compare et vote pour le chat qui juge le plus mignion et l'application lui propose aléatoirement un autre chat à la place. Les chats les plus mignions auront logiquement plus de vote.

- "/classement" Permet de visualiser le classement des chats trier par le plus grand nombre de vote.

## Api (https://cataas.com/) :

- Un appel unique à l'api lors du chargement initiale. Les données sont sauvegardées dans le localStorage.

- 502 requests (Attendre...)

- CORS requests ( Extention chrome : https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=fr)

Pour plus d'infos : thibaut.andre@ynov.com
